package Teste2;

import java.io.IOException;

import javax.servlet.http.Cookie;

import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.springframework.http.HttpRequest;

public class testeLeituraPagina2 {
	
	public static void main(String[] args) throws Exception {
		
		
		UnsupportedMimeTypeException mimeType = new UnsupportedMimeTypeException("Hey this is Mime",  "application/json", "http://dictionary.cambridge.org/dictionary/english/reality");
		String mime = mimeType.getMimeType();


		Document doc = Jsoup.connect("http://localhost:8088/desafio/funcionario/1/")
		    .requestBody("JSON")
		    .header("Content-Type", mime)
		    .ignoreContentType(true)
		    .post();
		System.out.println(doc);
		
		
	}

}
